/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"



//==============================================================================
/**
TODO 
-> RESET to implement
-> Auto compile in prepareToPlay method
-> Implement double precision processBlock (if useful)

TEST
->Check if parameters are saved (orc and score) -> seems ok

GUI 
->Smaller window (smaller console, equivalent orc/sco editor)
->Color syntax of csound
->Autocompletion ?
*/

//==============================================================================
CsoundTextVstAudioProcessor::CsoundTextVstAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
     #if ! JucePlugin_IsMidiEffect
     #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                       #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                       #endif
                     
#endif
) 

{    
    
    debug_mode=false;

    for(int i=1;i<21;i++) {
     std::string id="param" + std::to_string(i);
     std::string name="Parameter " + std::to_string(i);
     //debugMessage(String("parameter : ") + String(id.c_str()));
     PushParameter(id.c_str(),name.c_str(),0.0f,1.0f,0.0f);
    }

    
    if (supportsDoublePrecisionProcessing()) 
    {
     //setProcessingPrecision(AudioProcessor::doublePrecision);
    }
    
    //CSOUND THINGS
    cs=new Csound();
    is_ready=false;
    is_already_running=false;
    //debug via buffer
    cs->CreateMessageBuffer(0);
    compile_cnt=0;
    
    cs->SetHostImplementedMIDIIO(true);
    cs->SetHostImplementedAudioIO(1,0);
    cs->SetHostData(this);
 
    
    cs->SetOption("-n");
    cs->SetOption("-d");
    cs->SetOption("-b0");
    cs->SetOption("-M0");
    cs->SetOption("-m0d");
    cs->SetOption("--midi-key-cps=4");
    cs->SetOption("--midi-velocity-amp=5");
    
    cs->SetOption("-v");
    
    
//to implement -- from Cabbage

	cs->SetExternalMidiInOpenCallback(OpenMidiInputDevice);
	cs->SetExternalMidiReadCallback(ReadMidiData);
	cs->SetExternalMidiOutOpenCallback(OpenMidiOutputDevice);
    cs->SetExternalMidiWriteCallback(WriteMidiData);
    
    //cs->SetMessageCallback(csoundCallbackMessage);
    
    cs_params=nullptr;
    cs_params=new CSOUND_PARAMS();
    cs_params->displays=0;
    
    cs->SetParams(cs_params);

    /*  // THE FOLLOWING TECHNIQUE CAN BE USEFUL FOR PRESETS IF ANY
    PropertiesFile::Options opt;
    opt.applicationName="CsoundVstTextEditor";
    opt.filenameSuffix=".settings";
    opt.folderName="CsoundVstTextEditor";
    opt.osxLibrarySubFolder="Application Support/CsoundVstTextEditor";
    opt.commonToAllUsers=true;
    opt.doNotSave=false;
    opt.millisecondsBeforeSaving=0;
    

    appProperties.setStorageParameters(opt);
    
    PropertiesFile *propFile=appProperties.getUserSettings();
    if(propFile->containsKey("orc")) 
    {
        orc=propFile->getValue("orc","Failed to load orchestra from host");
    }
    if(propFile->containsKey("sco"))
    {
        sco=propFile->getValue("sco","Failed to load score from host");
    }
    */
    /*
    if(apvts.state.hasProperty("orc")) {
     var load_orc=apvts.state.getProperty("orc");
     if (load_orc.isString()) orc=load_orc.toString();
    }
    if(apvts.state.hasProperty("sco")) {
     var load_sco=apvts.state.getProperty("sco");
     if(load_sco.isString()) sco=load_sco.toString();
    }
    setValueTree();
    */
    
}

CsoundTextVstAudioProcessor::~CsoundTextVstAudioProcessor()
{
    delete cs;
}

//==============================================================================
const String CsoundTextVstAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool CsoundTextVstAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool CsoundTextVstAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool CsoundTextVstAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double CsoundTextVstAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int CsoundTextVstAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int CsoundTextVstAudioProcessor::getCurrentProgram()
{
    return 0;
}

void CsoundTextVstAudioProcessor::setCurrentProgram (int index)
{
}

const String CsoundTextVstAudioProcessor::getProgramName (int index)
{
    return {};
}

void CsoundTextVstAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void CsoundTextVstAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    String readystr=(is_ready==true) ? String("true") : String("false");
    debugMessage(String("Prepare to play -->") + readystr);
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    //buffer.setDataToReferTo(spout,Csound::GetNchnls(),0,Csound::GetKsmps()); 
    if (!is_ready) {
        debugMessage(String("Calling compile from prepare to play"));
        keep_running = CompileAndReadScore();
    }
    debugMessage(String("KSMPS : ") + String(csdKsmps));
    debugMessage(String("CSNDINDEX : ") + String(csndIndex));
    keep_running=true;


}

void CsoundTextVstAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    debugMessage(String("====RELEASE INSTANCE"));
    printCsoundBuffer();
    keep_running=false;
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool CsoundTextVstAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif


void CsoundTextVstAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    result=-1;
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    const int numSamples=buffer.getNumSamples();
    float** audioBuffers=buffer.getArrayOfWritePointers();
    MYFLT new_sample;
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

        
    //debugMessage(String("processblock float"));
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
     

    
         debugMessage(String("is ready : ") + (is_ready ? String("true") : String("false")));
                  debugMessage(String("keep running : ") + (keep_running ? String("true") : String("false")));
        if(!is_ready) return;
        if(!keep_running) return;
        
        debugMessage(String("going to process"));
        
        is_already_running=true;
        
        keyboardState.processNextMidiBuffer(midiMessages,0,numSamples,true);
        midiBuffer.addEvents(midiMessages,0,numSamples,0);

        
        // PARAMETERS, to restore
        for(int i=0;i<parameters.size();i++) {
        std::string s="param" + std::to_string(i+1);
         cs->SetChannel(s.c_str(),parameters.at(i)->get());
        }
        
        debugMessage(String("parameters processed"));

        for(int i=0;i<numSamples;i++,++csndIndex) 
        {
            if(csndIndex==csdKsmps) 
            {
             debugMessage(String("Going to perform ksmps"));
             result=cs->PerformKsmps(); 
             debugMessage(String("KSMPS performed"));
             //printCsoundBuffer();
                if(result!=0)
                    return;
                csndIndex=0;
            }

                pos=csndIndex * totalNumOutputChannels;
                for (auto channel = 0; channel < totalNumOutputChannels; ++channel)
                {
                    //index to start of channel buffer
                    float*& current_sample=audioBuffers[channel];
                    new_sample= *current_sample;
        
                    //critical part - accessing csound buffers
                    cs_spin[pos]=(double)new_sample;
                    *current_sample=(float)(cs_spout[pos]);
                    //debugMessage(String("Current Sample : ") + String(*current_sample));
        
                    //*current_sample=*current_sample * 0.1f;
                    ++current_sample;
                    ++pos;
                    // ..do something to the data...
                }
        }
}


void CsoundTextVstAudioProcessor::processBlock (AudioBuffer<double>& buffer, MidiBuffer& midiMessages)
{
    result=-1;
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    const int numSamples=buffer.getNumSamples();
    double** audioBuffers=buffer.getArrayOfWritePointers();
    MYFLT new_sample;
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    
        //debugMessage(String("processblock double"));

    
    
    //NOT IMPLEMENTED OR TESTED YET
        if(!is_ready) return;
        if(!keep_running) return;
        
        is_already_running=true;
        
        keyboardState.processNextMidiBuffer(midiMessages,0,numSamples,true);
        midiBuffer.addEvents(midiMessages,0,numSamples,0);
        
        for(int i=0;i<parameters.size();i++) {
        std::string s="param" + std::to_string(i+1);
         cs->SetChannel(s.c_str(),parameters.at(i)->get());
        }
        
        for(int i=0;i<numSamples;i++,++csndIndex) 
        {
            if(csndIndex==csdKsmps) 
            {
            
             result=cs->PerformKsmps();   

             printCsoundBuffer();
                if(result!=0)
                    return;
                csndIndex=0;
            }

                pos=csndIndex * totalNumOutputChannels;
                for (auto channel = 0; channel < totalNumOutputChannels; ++channel)
                {
                    //index to start of channel buffer
                    double*& current_sample=audioBuffers[channel];
                    new_sample= *current_sample;
        
                    //critical part - accessing csound buffers
                    cs_spin[pos]=new_sample;
                    *current_sample=cs_spout[pos];
                    debugMessage(String("Current Sample : ") + String(*current_sample));
        
        
                    //*current_sample=*current_sample * 0.1f;
                    ++current_sample;
                    ++pos;
                    // ..do something to the data...
                }
        } 
        
}



//==============================================================================
bool CsoundTextVstAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* CsoundTextVstAudioProcessor::createEditor()
{
    return new CsoundTextVstAudioProcessorEditor (*this);
}

//==============================================================================
void CsoundTextVstAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    
    /*
    ScopedPointer<XmlElement> xml(apvts.state.createXml());
    copyXmlToBinary(*xml,destData);*/
    
    std::unique_ptr<XmlElement> xml(new XmlElement("csound_data"));
    xml->setAttribute("orc",orc);
    xml->setAttribute("sco",sco);
    copyXmlToBinary(*xml,destData);
    
}

void CsoundTextVstAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    
    /*
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data,sizeInBytes));
    if(xmlState.get()!=nullptr)
        if(xmlState->hasTagName(apvts.state.getType()))
            apvts.replaceState(ValueTree::fromXml(*xmlState));
            */
            std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data,sizeInBytes));
            if(xmlState.get()!=nullptr) {
                if(xmlState->hasTagName("csound_data")) {
                 orc=xmlState->getStringAttribute("orc","failed to load orc");
                 sco=xmlState->getStringAttribute("sco","failed to load score");
                }
            }
                
                    

}



void CsoundTextVstAudioProcessor::PushParameter(const char *id,const char *name,float min,float max,float cur) 
{
    parameters.push_back(new AudioParameterFloat(String(id),String(name),min,max,cur));
    addParameter(parameters.back());
}


//Method called from plugin editor (button compile and read)
int CsoundTextVstAudioProcessor::SetOrcAndScore(String orch,String score, bool compile)
{
    orc=orch;
    sco=score;
    //is_ready=(cs->CompileOrc(orc.toStdString().c_str())==0 && cs->ReadScore(sco.toStdString().c_str())==0 && cs->Start()==0) ? true : false;
    debugMessage(String("SET ORC AND SCORE"));
    if(compile) {
        resetCsound();
        keep_running = CompileAndReadScore();
    }
    return result;
}

void CsoundTextVstAudioProcessor::resetCsound()
{
    is_ready = false;
    keep_running = false;
    cs->Stop();
    delete cs;
    //CSOUND THINGS
    cs=new Csound();
    is_ready=false;
    is_already_running=false;
    //debug via buffer
    cs->CreateMessageBuffer(0);
    compile_cnt=0;
    
    cs->SetHostImplementedMIDIIO(true);
    cs->SetHostImplementedAudioIO(1,0);
    cs->SetHostData(this);
 
    
    cs->SetOption("-n");
    cs->SetOption("-d");
    cs->SetOption("-b0");
    cs->SetOption("-M0");
    cs->SetOption("-m0d");
    cs->SetOption("--midi-key-cps=4");
    cs->SetOption("--midi-velocity-amp=5");
    
    cs->SetOption("-v");
    
    
//to implement -- from Cabbage

	cs->SetExternalMidiInOpenCallback(OpenMidiInputDevice);
	cs->SetExternalMidiReadCallback(ReadMidiData);
	cs->SetExternalMidiOutOpenCallback(OpenMidiOutputDevice);
    cs->SetExternalMidiWriteCallback(WriteMidiData);
}

bool CsoundTextVstAudioProcessor::CompileAndReadScore() 
{   
        
    
    
        debugMessage(String("Orchestra : ") + orc);
        debugMessage(String("Score : ") + sco);
        compile_cnt++;
        debugMessage(String("COMPILE NUMBER ===> ") + String(compile_cnt));
        

        
        result=cs->CompileOrc(orc.toStdString().c_str());
        is_ready=AssertResult(String("COMPILE"));
        printCsoundBuffer();
        if(result!=0) {
            printCsoundBuffer();
            return false;
        }
        
        result=cs->ReadScore(sco.toStdString().c_str());
        is_ready=AssertResult(String("READ SCORE"));
        printCsoundBuffer();
        if(result!=0) {
            printCsoundBuffer();
            return false;
        }
        
        setValueTree();
        /*
        PropertiesFile *propFile=appProperties.getUserSettings();
        propFile->setValue("orc",orc);
        propFile->setValue("sco",sco);
        */
        
        if(is_already_running) return true;
        
        result = cs->Start();
        
        is_ready=AssertResult(String("START CSOUND"));
        
        
        printCsoundBuffer();
        if(result!=0) {
            printCsoundBuffer();
            return false;
        }
        
        cs->SetHostData(this);
        
        csdKsmps=cs->GetKsmps();
        cs_spout=cs->GetSpout();
        cs_spin=cs->GetSpin();
        cs_scale=cs->Get0dBFS();
        csndIndex=cs->GetKsmps();
        printCsoundBuffer();
        debugMessage(String("KSMPS : ") + String(csdKsmps));
        debugMessage(String("CSNDINDEX : ") + String(csndIndex));
        
        return true;
        //is_already_running = true;

}



void CsoundTextVstAudioProcessor::setValueTree()
{
    /*
    ValueTree vt=apvts.state.getOrCreateChildWithName("csound_csd_data",undoManager);
        vt.setProperty("orc",orc,undoManager);
        vt.setProperty("sco",sco,undoManager);
        vt.setProperty("console",cs_msg.c_str(),undoManager);
        
        */
    
    /*
        valueTree.setProperty("orc",orc,undoManager);
        valueTree.setProperty("sco",sco,undoManager);
        valueTree.setProperty("console",cs_msg.c_str(),undoManager);
        apvts.state=valueTree;
        
*/    

}



AudioProcessorValueTreeState::ParameterLayout CsoundTextVstAudioProcessor::createParameterLayout() {
    std::vector<std::unique_ptr<AudioParameterFloat>> params;
    for (int i = 1; i < 21; ++i) {
        std::string s="param" + std::to_string(i);
        params.push_back (std::make_unique<AudioParameterFloat> (String(s.c_str()),String(s.c_str()), 0.0f, 1.0f, 0.0f));
    }
    return { params.begin(), params.end() };
}


String CsoundTextVstAudioProcessor::getOrc()
{
return orc;
}


String CsoundTextVstAudioProcessor::getScore()
{
return sco;
}


void CsoundTextVstAudioProcessor::StopCsound()
{

}

//CSOUND MIDI FUNCTIONS 
int CsoundTextVstAudioProcessor::OpenMidiInputDevice(CSOUND* csnd,void **userData,const char *devName) 
{
 *userData=csoundGetHostData(csnd);
 return 0;
}



int CsoundTextVstAudioProcessor::ReadMidiData(CSOUND* csnd,void *userData,unsigned char *mbuf,int nbytes)
{
 CsoundTextVstAudioProcessor *midiData=(CsoundTextVstAudioProcessor*)userData;
 if(!userData) {
      //displayMessage("Read Midi data Invalid\n");   
     return 0;
 }
 int cnt=0;
 if(!midiData->midiBuffer.isEmpty() && cnt<=(nbytes-3))
    {
     MidiMessage message(0xf4,0,0,0);
     MidiBuffer::Iterator i(midiData->midiBuffer);
     int messageFrameRelativeTothisProcess;
     while(i.getNextEvent(message,messageFrameRelativeTothisProcess))
        {
         const uint8* data=message.getRawData();

         *mbuf++=*data++;
         if(message.isChannelPressure() || message.isProgramChange())
            {
                *mbuf++=*data++;
                cnt+=2;
            } else {
             *mbuf++=*data++;
             *mbuf++=*data++,
             cnt+=3;
            }
        }
        midiData->midiBuffer.clear();
    }
    return cnt;
}


int CsoundTextVstAudioProcessor::OpenMidiOutputDevice(CSOUND* csnd,void **userData,const char *devName)
{
 *userData=csoundGetHostData(csnd);
 return 0;
}

int CsoundTextVstAudioProcessor::WriteMidiData(CSOUND* csnd,void* _userData,const unsigned char* mbuf,int nbytes)
{
 CsoundTextVstAudioProcessor* userData=(CsoundTextVstAudioProcessor*)_userData;
 if(!userData) {
    //displayMessage("Write Midi data Invalid\n");
     return 0;
 }
 MidiMessage message(mbuf,nbytes,0);
 userData->midiOutputBuffer.addEvent(message,0);
 return nbytes;
}


//DEBUG FUNCTIONS

bool CsoundTextVstAudioProcessor::AssertResult(String indice)
{
 if(result==0) { 
    debugMessage(indice + String(" -- RESULT ZERO -- EVERYTHING OK "));
    printCsoundBuffer();
    return true;
 } else {
    debugMessage(indice + String(" --RESULT IS FALSE : ") + String(result));
    printCsoundBuffer();
    return false;
 }    
}


const char* CsoundTextVstAudioProcessor::getCsoundMessages()
{    
    const char *msg=cs_msg.c_str();
    return msg;
}

void CsoundTextVstAudioProcessor::clearMessage()
{
    cs_msg.clear();   
}


void CsoundTextVstAudioProcessor::printCsoundBuffer() {
    //std::ofstream ofs(DEBUG_FILE_PATH, std::ios::out | std::ios::app );
            while(cs->GetMessageCnt() > 0) 
        {
            const char *msg=cs->GetFirstMessage();
            //ofs << msg;
            //ofs << "\n";
            cs_msg+=msg;
            cs->PopFirstMessage();   
        }
        
            //ofs.close();
}


void CsoundTextVstAudioProcessor::displayMessage(String msg)
{
    cs_msg+=msg.toStdString() + "\n";
}

void CsoundTextVstAudioProcessor::debugMessage(String msg) 
{    
    if(!debug_mode) return;
    
    std::ofstream ofs(DEBUG_FILE_PATH, std::ios::out | std::ios::app );
    ofs << msg;
    ofs << "\n";
    ofs.close();
    
    displayMessage(msg);
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new CsoundTextVstAudioProcessor();
}
