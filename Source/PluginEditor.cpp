/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CsoundTextVstAudioProcessorEditor::CsoundTextVstAudioProcessorEditor (CsoundTextVstAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p), scoEditor(scoDoc, &tokenizer), orcEditor(orcDoc, &tokenizer)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (1000, 750);
    //tabs
    //console 
    console.setMultiLine(true);
    console.setScrollbarsShown(true);
    console.setReadOnly(true);
    addAndMakeVisible(&console);
    //Score and Orchestra  editor for csound
    /*
    orcEditor.setMultiLine(true);
    scoEditor.setMultiLine(true);
    orcEditor.setScrollbarsShown(true);
    scoEditor.setScrollbarsShown(true);
    orcEditor.setReturnKeyStartsNewLine(true);
    scoEditor.setReturnKeyStartsNewLine(true);
    orcEditor.setTabKeyUsedAsCharacter(true);
    scoEditor.setTabKeyUsedAsCharacter(true);
    orcEditor.setCaretVisible(true);
    scoEditor.setCaretVisible(true);
    */
    
    //orcEditor.addListener(this);
    //scoEditor.addListener(this);
    
    double sr=processor.getSampleRate();
    if(sr==0 || sr<44100) sr=44100;
    int ksmps=processor.getBlockSize();
    
    //processor.setProcessingPrecision(AudioProcessor::doublePrecision);
    if(processor.getOrc().length()>0 &&  processor.getScore().length()>0) {
      //orcEditor.setText(processor.getOrc(),false);
      //scoEditor.setText(processor.getScore(),false);
      orcDoc.replaceAllContent(processor.getOrc());
      scoDoc.replaceAllContent(processor.getScore());
    } else {
    String orc_predef="//csound orchestra -- make sure sample rate corresponds to DAW sample rate \nsr=" + String(sr) + " \nksmps=" + String(ksmps) + " \nnchnls=2 \n0dbfs=1 \n";
    if (processor.isUsingDoublePrecision())
        orc_predef="//This DAW supports double processing precision\n"+orc_predef;
    
    //orc_predef+="\ninstr 1 \nain inch 1 \nkp chnget \"param1\" \nkfq=(kp*800)+50 \naout moogvcf ain,kfq,0.7 \nouts aout,aout \nendin \n";
    orc_predef+="\n" + midiOrc;
    
    orcDoc.replaceAllContent(orc_predef);
    //orcEditor.setText(orc_predef,false);
    String sco_comment="//csound score -- for an audio effect use negative p3 \n";
    sco_comment+="\nf 0 z \n\n//i 1.1 0 -1 \n";
    scoDoc.replaceAllContent(sco_comment);
    //scoEditor.setText(sco_comment,false);
    }
    //Compile and stop buttons
    playBut.setButtonText("Compile and run");
    playBut.addListener(this);


    addAndMakeVisible(&playBut);
    addAndMakeVisible(&orcEditor);
    addAndMakeVisible(&scoEditor);
    
    
    SetOrcAndScore(false);
    scoEditor.setColourScheme(getColourScheme());
    orcEditor.setColourScheme(getColourScheme());
}

CsoundTextVstAudioProcessorEditor::~CsoundTextVstAudioProcessorEditor()
{
    playBut.removeListener(this);
}

//==============================================================================
void CsoundTextVstAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
    g.setColour (Colours::white);
    g.setFont (28.4757f);
    g.drawFittedText ("Hello World!", getLocalBounds(), Justification::centred, 1);
}

void CsoundTextVstAudioProcessorEditor::resized()
{
    int leftBound=25;
    int highBound=25;
    
    int editorHeight=(getHeight()/2)-150;
    int editorWidth=getWidth()-160;
    
    
    playBut.setBounds(leftBound+editorWidth + 10,highBound,80,40);
    
    orcEditor.setBounds(leftBound,highBound,editorWidth,editorHeight);
    scoEditor.setBounds(leftBound,highBound+editorHeight + 20,editorWidth,editorHeight);
    console.setBounds(leftBound,highBound+(editorHeight*2) + 40,editorWidth,150);

}

// text editor listeners
void CsoundTextVstAudioProcessorEditor::textEditorReturnKeyPressed(TextEditor &t) {
    std::cout << "Return pressed " << std::endl;
}

void CsoundTextVstAudioProcessorEditor::textEditorEscapeKeyPressed(TextEditor &t) {
    std::cout << "Escape pressed " << std::endl;
}

void CsoundTextVstAudioProcessorEditor::textEditorTextChanged(TextEditor &t) {
}

void CsoundTextVstAudioProcessorEditor::timerCallback() 
{
    //to change with insert at caret so i can clear the string in processor (less memory consumption)
     const char * msg=processor.getCsoundMessages();
     //console.setText(msg,true);
     console.insertTextAtCaret(msg);
     console.moveCaretToEnd();     
     processor.clearMessage();
}


//ButtonListeners
void CsoundTextVstAudioProcessorEditor::buttonClicked(Button *b) {
    if(b==&playBut)
        SetOrcAndScore(true);
}


void CsoundTextVstAudioProcessorEditor::SetOrcAndScore(bool compile)
{  

    String orc=orcDoc.getAllContent();
    //String orc=orcVal.toString();
    String sco=scoDoc.getAllContent();
    //String sco=scoVal.toString();
    
    
    if(processor.SetOrcAndScore(orc,sco,compile)==0)
    {
        //startTimer(100);
    }
    
    timerCallback();
    
    
}

CodeEditorComponent::ColourScheme CsoundTextVstAudioProcessorEditor::getColourScheme()
{
     struct Type
    {
        const char* name;
        uint32 colour;
    };

        
        const Type types[] =
        {
            { "Error",              0xffe60000 },
            { "Comment",            0xff0cd270 },
            { "Keyword",            0xffee6fd4 },
            { "Operator",           0xffeffcfd },
            { "Identifier",         0xff3ee859 },
            { "Integer",            0xff4aeee9 },
            { "Float",              0xff42c8c4 },
            { "String",             0xffc81df8 },
            { "Bracket",            0xff058202 },
            { "Punctuation",        0xffcfbeff },
            { "Preprocessor Text",  0xfff8f631 }
        };

        CodeEditorComponent::ColourScheme cs;

        for (std::size_t i = 0;
             i < sizeof (types) / sizeof (types[0]); ++i) // (NB: numElementsInArray doesn't work here in GCC4.2)
            cs.set (types[i].name, Colour (types[i].colour));

        return cs;
}



