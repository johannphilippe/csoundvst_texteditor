/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include<queue>
#include"CsoundCodeTokenizer.h"
//#include"CsoundHighlight.h"

//==============================================================================
/**
*/



class CsoundTextVstAudioProcessorEditor  : public AudioProcessorEditor, public TextEditor::Listener, public TextButton::Listener, public Timer
{
public:
    CsoundTextVstAudioProcessorEditor (CsoundTextVstAudioProcessor&);
    ~CsoundTextVstAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    
    void textEditorReturnKeyPressed(TextEditor &) override;
    void textEditorEscapeKeyPressed(TextEditor &) override;
    void textEditorTextChanged(TextEditor &) override;
    
    void buttonClicked(Button *) override;
    
    void SetOrcAndScore(bool compile);

    void timerCallback();


    CodeEditorComponent::ColourScheme getColourScheme();

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    CsoundTextVstAudioProcessor& processor;
    
    //Slider midiVolume;
    CsoundTokeniser tokenizer;
    CodeDocument orcDoc, scoDoc;
    CodeEditorComponent orcEditor, scoEditor;
    TextEditor /*orcEditor,scoEditor, */console;
    //Buttons
    TextButton playBut;

    
    //CSD's for test
    const String midiOrc="instr 1 \niFreq=p4 \niAmp=p5 \naout vco2 iAmp,iFreq \nouts aout * 0.2,aout * 0.2 \nendin";
    
    //TABS AND FILE BROWSER

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CsoundTextVstAudioProcessorEditor)
};
