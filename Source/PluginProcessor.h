/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#define DEBUG_FILE_PATH "/Volumes/AUDIO/Johann/debug_csoundvst.txt"

#include "../JuceLibraryCode/JuceHeader.h"


#ifdef JUCE_MAC
#include"csound.h"
#include"csound.hpp"
#else
#include "csound/csound.h"
#include "csound/csound.hpp"
#endif


#include<iostream>
#include<fstream>
#include<queue>
#include<mutex>

//==============================================================================
/**
*/
class CsoundTextVstAudioProcessor  : public AudioProcessor, public Csound
{
public:
    //==============================================================================
    CsoundTextVstAudioProcessor();
    ~CsoundTextVstAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages) override;
    void processBlock (AudioBuffer<double>& buffer, MidiBuffer& midiMessages) override;

    //void processBlock (AudioBuffer<double>& audioBuf, MidiBuffer& midiBuf) override;
    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    //Automations parameters
    void PushParameter(const char *id,const char *name,float min,float max,float cur);
    
    //Csound Methods -- also compiles and read score
    int SetOrcAndScore(String orc, String sco, bool compile);
    
    bool CompileAndReadScore();
    void StopCsound();
    
    
    String getOrc();
    String getScore();
    
    //CSound API functions for midi
    static int OpenMidiInputDevice(CSOUND *csnd,void **userData,const char *devName);
    static int OpenMidiOutputDevice(CSOUND *csnd,void **userData,const char *devName);
    static int ReadMidiData(CSOUND* csound,void *userData,unsigned char *mbuf,int nbytes);
    static int WriteMidiData(CSOUND *csnd,void *userData,const unsigned char *mbuf,int nbytes);
    
    
    //utilities
    bool AssertResult(String indice);
    void printCsoundBuffer();
    void displayMessage(String msg);
    void debugMessage(String msg);
    
    
    
    AudioProcessorValueTreeState::ParameterLayout createParameterLayout();
    void setValueTree();

    
    //console method
    const char* getCsoundMessages();
    void clearMessage();
    
    
    void resetCsound();
private:
    //Csound
    Csound *cs;
    bool debug_mode;
    std::atomic<bool> is_ready;
    std::atomic<bool> is_already_running;
    std::atomic<bool> keep_running;
    std::atomic<int> result;
    MYFLT *cs_spout, *cs_spin;
    MYFLT cs_scale;
    int csdKsmps,csndIndex;
    int samplingRate=44100;
    int pos;
    ScopedPointer<CSOUND_PARAMS> cs_params;
    
    //MIDI Private members
    MidiBuffer midiOutputBuffer;
    MidiBuffer midiBuffer;
    MidiKeyboardState keyboardState;
    
    //orc sco
    String orc,sco;

    
    
    //dbg file
    FILE *dbgfile;
    String dbg;
    int compile_cnt;

    
    //JUCE PARAMETERS -- AUTOMATIONS
    std::vector<AudioParameterFloat *> parameters;
    std::vector<float *> param_ptr;
    
    
    //csound message string
    std::string cs_msg;
    
    //VALUE TREE
    //AudioProcessorValueTreeState apvts;
    ValueTree valueTree;
    UndoManager *undoManager;
    
    //ApplicationProperties appProperties;
    

    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CsoundTextVstAudioProcessor)
};


