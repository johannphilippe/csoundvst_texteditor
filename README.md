# CsoundVST_TextEditor    

This project is a simple VST program made with JUCE, and designed to embed Csound programming language inside the DAW.   
It should work with VST3 on Windows (tested with Reaper and Max Msp) and Mac OSX (not tested yet). 
This plugin currently works as a x64 VST, so x64 version of Csound is required. 

# Library

- It uses Csound 6
- It borrows a little bit of Code from [Cabbage](https://github.com/rorywalsh/cabbage) in the file CsoundCodeTokenizer.h 

# How it works   

There are two text editors : one for orchestra, one for score. You can write anything Csound may work with.    
For now, it's only working with 2 channels (2 ins, 2outs), but it'll be available with multichannel soon.   
It also works as a MIDI triggered sythesizer (note num as p4, velocity as p5).   
To get the audio input from the track (or item in Reaper) - the opcode inch can be used (inch 1 for input 1).    
To get automation parameters, use "chnget" opcode -> there are 20 parameters scaled from 0 to 1. Writing : kp chnget "param1" will push the value of "Parameter 1" automation parameter in the "kp" value at k-rate.   

-Import orchestra / score : you can import csound files (csd, orc,sco) with the browse button.    
The current orchestra / score for each csound VST will be saved with you DAW session.    

The "compile and start" button is mostly useful for checking debug informations in the console (third text field) and recompile a score/orchestra. For now, the "stop" button is useless, but it'll be usable soon.    

# Features to come    

-Multichannel input/output.     
